from sys import argv

from pip._vendor.distlib.compat import raw_input

script,filename = argv

print(f"We're going to erase {filename}.")
print("If you don't want that, hit CTRL-c (^C).")
print("If you do want that, hit RETURN")

input("?")

print("Opening the file...")
target=open(filename,'w')

#print("Truncating the file, Goodbye!")
#target.truncate()

print("Now I'm going to ask you for three lines.")

line1,line2,line3=raw_input("Line 1:"),raw_input("Line 2:"),raw_input("Line 3:")

print("I'm going to write these to the file.")

target.write(f"{line1}\n{line2}\n{line3}")


print("And finally, we close it")

target.close()