#initializing variable car
cars=100
#initializing variable space in a car
space_in_a_car=4.0
#initializing variable drivers
drivers=30
#initializng passengers
passengers=90
#initializing cars not driven
cars_not_driven=cars-drivers
#initializng cars driven
cars_driven=drivers
#initializing carpool capacity
carpool_capacity=cars_driven*space_in_a_car
#intializing average passengers per car
average_passengers_per_car=passengers/cars_driven


#printing cars
print("There are",cars,"cars available")
#printing drivers
print("There are only", drivers, "drivers available.")
#printing cars not driven
print("There will be", cars_not_driven,"empty cars today")
#printing capacity
print("We can transport", carpool_capacity, "people today")
#printing passengers
print("We have", passengers, "to carpool today." )
#printing average
print("We need to put about", average_passengers_per_car, "in each car.")
