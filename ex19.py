#definition of function
def cheese_and_crackers(cheese_count,boxes_of_crackers):
    #printing cheese count
    print(f"You have {cheese_count} cheeses!")
    #printing boxes
    print(f"You have {boxes_of_crackers} boxes of crackers!")
    #printing
    print("Man thas's enough for a party")
    print("Get a blanket.\n")

#printing string
print("We can just give the function numbers directly:")
#function call
cheese_and_crackers(20,30)

#printing and variable intialization
print("OR,we can use variables from our script:")
amount_of_cheese=10
amount_of_crackers=50

#function call with variable arguments
cheese_and_crackers(amount_of_cheese,amount_of_crackers)

#function call with math arguments
print("We can even do math inside too:")
cheese_and_crackers(10+20,5+6)

#printing with variable and math arguments
print("And we can combine the two,variables and math:")
cheese_and_crackers(amount_of_cheese*100,amount_of_crackers*1000)