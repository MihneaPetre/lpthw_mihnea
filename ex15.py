from sys import argv
#takes argument
script,filename=argv
#reads from file
txt = open(filename)
#print filename
print(f"Here's your file {filename}:")
#prints file content
print(txt.read())
#prints string
print("Type the filename again:")
#takes input into file_again
file_again = input("> ")
#open the file form input
txt_again = open(file_again)
#reads the file from input
print(txt_again.read())

txt.close()
txt_again.close()