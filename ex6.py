#types of people initialization
types_of_people = 10
#x initialization
x=f"There are {types_of_people} types of people."

#binary initialization
binary = "binary"
#do_not initialization
do_not = "don't"
#y initialization
y = f"Those who know {binary} and those who {do_not}."

#printing x
print(x)
#printing y
print(y)

#printing x
print(f"I said:{x}")
#printing y
print(f"I also said:'{y}'")

#hilarious initialization
hilarious = False

#joke evaluation initialization
joke_evaluation = "Isn't that joke so funny?!{}"

#printing joke evaluation with format
print(joke_evaluation.format(hilarious))

#w initialization
w = "This is the left side of..."
#e initialization
e = "a string with a right side."

#printing w and e
print(w+e)