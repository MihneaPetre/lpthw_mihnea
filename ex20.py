from sys import argv

script, input_file = argv

#prints the whole file
def print_all(f):
    print(f.read())

#it goes to the file first position
def rewind(f):
    f.seek(0)

#prints a number of lines
def print_a_line(line_count, f):
    print(line_count, f.readline())

#opens the file
current_file=open(input_file)


print("First let's print the whole file \n")
#prints the whole file
print_all(current_file)

print("Now let's rewind, kind of like a tape.")
#goest to the first line of the file
rewind(current_file)

print("Let's print three lines:")

current_line=1
#prints line 1
print_a_line(current_line, current_file)
#incrementing the line
current_line+= 1
print(current_line)
#prints line 2
print_a_line(current_line, current_file)
#incrementing the line
current_line+=1
print(current_line)
#prints the third line
print_a_line(current_line, current_file)